package com.pookidevs.tarkeeb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.Window
import android.widget.ImageView
import androidx.appcompat.widget.Toolbar
import androidx.viewpager.widget.ViewPager
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.tabs.TabLayout
import com.google.firebase.firestore.FirebaseFirestore
import com.pookidevs.tarkeeb.recipe.RecipePagerAdapter
import com.squareup.picasso.Picasso

class RecipeSingle : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        //requestWindowFeature(Window.FEATURE_NO_TITLE); //will hide the title
        setContentView(R.layout.activity_recipe_single)
        val mToolbar =  findViewById<Toolbar>(R.id.toolbarid)
        setSupportActionBar(mToolbar);
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true);
        getSupportActionBar()?.setDisplayShowHomeEnabled(true);

        val mAppBarLayout = findViewById<AppBarLayout> (R.id.appbar)
        mAppBarLayout.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { mAppBarLayout, i ->
            var isShow = false
            var scrollRange = -1
            fun onOffsetChanged(appBarLayout: AppBarLayout, verticalOffset: Int) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    isShow = true;
                } else if (isShow) {
                    isShow = false;
                }
            }
        })
        val recipeNameID = intent.extras.getString("name_id")

        val db = FirebaseFirestore.getInstance()
        //val toolbar = findViewById<CollapsingToolbarLayout>(R.id.collapsingtoolbar)

        val docRef = db.collection("Recipes").document(recipeNameID)
        docRef.get().addOnSuccessListener { documentSnapshot ->
            val recipe = documentSnapshot.toObject(Recipe::class.java)
            mToolbar.title = recipe?.name
            Picasso.get().load(recipe?.image).into(findViewById<ImageView> (R.id.expandedImage))
        }

        val recipePagerAdapter = RecipePagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = recipePagerAdapter
        val tabs: TabLayout = findViewById(R.id.recipe_tabs)
        tabs.setupWithViewPager(viewPager)
    }

    override fun onSupportNavigateUp():Boolean {
        onBackPressed();
        return true;
    }
}
