package com.pookidevs.tarkeeb.ui.main


import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import com.pookidevs.tarkeeb.R
import com.pookidevs.tarkeeb.SingleCategoryActivity
import kotlinx.android.synthetic.main.fragment_category.view.*


class CategoryFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        var rootView =  inflater.inflate(R.layout.fragment_category, container, false)

        val mainCourseCard = rootView.main_course_card
        val bbqCard = rootView.bbq_card
        val dessertsCard = rootView.dessert_card
        val cakesCard = rootView.cakes_card
        val beveragesCard = rootView.beverages_card
        val fastFoodCard = rootView.fast_food_card

        mainCourseCard.setOnClickListener {
            val categoryIntent = Intent(context, SingleCategoryActivity::class.java)
            categoryIntent.putExtra("category_name", "Main Course")
            categoryIntent.putExtra("category_id", "main_course")
            startActivity(categoryIntent)
        }

        bbqCard.setOnClickListener {
            val categoryIntent = Intent(context, SingleCategoryActivity::class.java)
            categoryIntent.putExtra("category_name", "BBQ")
            categoryIntent.putExtra("category_id", "bbq")
            startActivity(categoryIntent)
        }

        dessertsCard.setOnClickListener {
            val categoryIntent = Intent(context, SingleCategoryActivity::class.java)
            categoryIntent.putExtra("category_name", "Dessert")
            categoryIntent.putExtra("category_id", "dessert")
            startActivity(categoryIntent)
        }

        cakesCard.setOnClickListener {
            val categoryIntent = Intent(context, SingleCategoryActivity::class.java)
            categoryIntent.putExtra("category_name", "Cakes and Cookies")
            categoryIntent.putExtra("category_id", "cakes_and_cookies")
            startActivity(categoryIntent)
        }

        beveragesCard.setOnClickListener {
            val categoryIntent = Intent(context, SingleCategoryActivity::class.java)
            categoryIntent.putExtra("category_name", "Beverages")
            categoryIntent.putExtra("category_id", "beverages")
            startActivity(categoryIntent)
        }

        fastFoodCard.setOnClickListener {
            val categoryIntent = Intent(context, SingleCategoryActivity::class.java)
            categoryIntent.putExtra("category_name", "Fast Food")
            categoryIntent.putExtra("category_id", "fast_food")
            startActivity(categoryIntent)
        }

        return rootView
    }

}
