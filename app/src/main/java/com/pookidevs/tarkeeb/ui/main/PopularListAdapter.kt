package com.pookidevs.tarkeeb.ui.main

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.pookidevs.tarkeeb.R
import com.pookidevs.tarkeeb.RecipeShort
import com.pookidevs.tarkeeb.RecipeSingle
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.recipe_list_card.view.*

class PopularListAdapter (val items : ArrayList<RecipeShort>, val context: Context) : RecyclerView.Adapter<PopularListAdapter.ViewHolder>(){


    private val db = FirebaseFirestore.getInstance()

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.recipeName.text = items.get(position).name
        holder.category1.text = items.get(position).category
        holder.category2.text = items.get(position).category
        Picasso.get().load(items.get(position).image).into(holder.recipeImage)
        holder.saveButton.setOnClickListener{
            val uid = FirebaseAuth.getInstance().getCurrentUser()!!.getUid();
            db.collection("Saved").document(uid).collection("Recipes").document(items.get(position).nameID).set(items.get(position))
            Toast.makeText(context, "Saved recipe to My List", Toast.LENGTH_SHORT).show()
        }

        holder.parent.setOnClickListener {
            val recipeIntent = Intent(context, RecipeSingle::class.java)
            recipeIntent.putExtra("name_id", items.get(position).nameID)
            recipeIntent.putExtra("name", items.get(position).name)
            context.startActivity(recipeIntent)
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.recipe_list_card,
                parent,
                false
            )
        )
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each recipe to
        val recipeName = view.recipeName
        val category1 = view.category1
        val category2 = view.category2
        val parent = view.listItemParent
        val recipeImage = view.recipeImage
        val saveButton = view.saveButton
    }

}