package com.pookidevs.tarkeeb.ui.main

import android.content.ContentValues.TAG
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore
import com.pookidevs.tarkeeb.R
import com.pookidevs.tarkeeb.RecipeShort
import kotlinx.android.synthetic.main.fragment_popular.view.*

class PopularFragment : Fragment() {



    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val rootView = inflater.inflate(R.layout.fragment_popular, container, false)

        val progressBar = rootView.progressBar

        val recipes: ArrayList<RecipeShort> = ArrayList()
        //load recipe_shorts from DB
        val db = FirebaseFirestore.getInstance()

        db.collection("RecipesShort")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    recipes.add(document.toObject(RecipeShort::class.java))
                }
                rootView.recipesRecycler.layoutManager = LinearLayoutManager(this.context)
                rootView.recipesRecycler.adapter = PopularListAdapter(recipes,this.context!!)
                progressBar.visibility = View.GONE
            }
            .addOnFailureListener { exception ->
                Log.d(TAG, "Error getting documents: ", exception)
            }
        // Inflate the layout for this fragment
        return rootView
    }
}