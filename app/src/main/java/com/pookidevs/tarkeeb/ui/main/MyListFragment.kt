package com.pookidevs.tarkeeb.ui.main

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.pookidevs.tarkeeb.R
import com.pookidevs.tarkeeb.RecipeShort
import kotlinx.android.synthetic.main.fragment_mylist.*
import kotlinx.android.synthetic.main.fragment_mylist.view.*

class MyListFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val rootView = inflater.inflate(R.layout.fragment_mylist, container, false)

        var progressBar = rootView.progressBar

        val recipes: ArrayList<RecipeShort> = ArrayList()
        //load recipe_shorts from DB
        val db = FirebaseFirestore.getInstance()
        val uid = FirebaseAuth.getInstance().getCurrentUser()!!.getUid()
        db.collection("Saved").document(uid).collection("Recipes")
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    recipes.add(document.toObject(RecipeShort::class.java))
                }
                rootView.my_list_fragment.layoutManager = LinearLayoutManager(this.context)
                rootView.my_list_fragment.adapter = PopularListAdapter(recipes,this.context!!)
                progressBar.visibility = View.GONE
            }
            .addOnFailureListener { exception ->
                Log.d("D", "Error getting documents", exception)
            }

        return rootView
    }
}
