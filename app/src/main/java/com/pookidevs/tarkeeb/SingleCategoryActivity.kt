package com.pookidevs.tarkeeb

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ProgressBar
import android.widget.TextView
import androidx.appcompat.widget.Toolbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.firebase.firestore.FirebaseFirestore
import com.pookidevs.tarkeeb.ui.main.PopularListAdapter

class SingleCategoryActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_single_category)
        val mToolbar =  findViewById<Toolbar>(R.id.toolbarid)
        setSupportActionBar(mToolbar)
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)
        var title = intent.getStringExtra("category_name")
        mToolbar.title = title
        val title_tv = findViewById<TextView>(R.id.category_name)
        title_tv.setText(title)

        val progressBar = findViewById<ProgressBar>(R.id.progressBar)

        val recipesRecycler = findViewById<RecyclerView>(R.id.category_recipe_recycler)
        val db = FirebaseFirestore.getInstance()
        val recipes: ArrayList<RecipeShort> = ArrayList()
        //val capitalCities = db.collection("cities").whereEqualTo("capital", true)
        val categoryID = intent.getStringExtra("category_id")
        db.collection("RecipesShort").whereEqualTo("categoryID",categoryID)
            .get()
            .addOnSuccessListener { result ->
                for (document in result) {
                    recipes.add(document.toObject(RecipeShort::class.java))
                }
                recipesRecycler.layoutManager = LinearLayoutManager(this)
                recipesRecycler.adapter = PopularListAdapter(recipes,this)
                progressBar.visibility = View.GONE
            }
            .addOnFailureListener { exception ->
                Log.d("D", "Error getting documents: ", exception)
            }
    }

    override fun onSupportNavigateUp():Boolean {
        onBackPressed();
        return true;
    }
}
