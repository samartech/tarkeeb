package com.pookidevs.tarkeeb.recipe

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pookidevs.tarkeeb.R
import kotlinx.android.synthetic.main.step_list_card.view.*

class StepsListAdapter(val items : ArrayList<String>, val context: Context) : RecyclerView.Adapter<StepsListAdapter.ViewHolder>(){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.step.text = (position+1).toString()+ ". " + items.get(position)
    }

    override fun getItemCount(): Int {
        return items.size;
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.step_list_card,
                parent,
                false
            )
        )
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each recipe to
        val step = view.step
    }

}