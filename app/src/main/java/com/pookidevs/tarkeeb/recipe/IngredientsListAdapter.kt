package com.pookidevs.tarkeeb.recipe

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.pookidevs.tarkeeb.R

import kotlinx.android.synthetic.main.ingredient_list_card.view.*


class IngredientsListAdapter (val items : ArrayList<String>, val context: Context) : RecyclerView.Adapter<IngredientsListAdapter.ViewHolder>(){

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val split = items.get(position).split('/')
        if(split.size == 2) {
            holder.ingredientName.text = split[0]
            holder.ingredientQty.text = split[1]
        }
        else{
            holder.ingredientName.text = split[0]
            holder.ingredientQty.text = ""
        }
    }

    override fun getItemCount(): Int {
        return items.size;
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(context).inflate(
                R.layout.ingredient_list_card,
                parent,
                false
            )
        )
    }

    class ViewHolder (view: View) : RecyclerView.ViewHolder(view) {
        // Holds the TextView that will add each recipe to
        val ingredientName = view.ingredientName
        val ingredientQty = view.ingredientQty
    }

}