package com.pookidevs.tarkeeb.recipe

import android.content.ContentValues
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.firebase.firestore.FirebaseFirestore
import com.pookidevs.tarkeeb.R
import com.pookidevs.tarkeeb.Recipe
import kotlinx.android.synthetic.main.fragment_steps.view.*

class StepsFragment : Fragment() {


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        val rootView  = inflater.inflate(R.layout.fragment_steps, container, false)

        var steps = ArrayList<String>()
        //load recipe_shorts from DB
        val db = FirebaseFirestore.getInstance();

        var recipe_name_id = activity?.intent?.extras?.getString("name_id")

        if(recipe_name_id == null){
            recipe_name_id=""
        }
        db.collection("Recipes").document(recipe_name_id)
            .get().addOnSuccessListener { documentSnapshot ->
                val recipe = documentSnapshot.toObject(Recipe::class.java)?: Recipe()
                for (i in recipe.steps) {
                    steps.add(i)
                }
                rootView.stepsRecycler.layoutManager = LinearLayoutManager(this.context)
                rootView.stepsRecycler.adapter = StepsListAdapter(steps,this.context!!)
            }
            .addOnFailureListener { exception ->
                Log.d(ContentValues.TAG, "Error getting documents: ", exception)
            }

        return rootView
    }


}