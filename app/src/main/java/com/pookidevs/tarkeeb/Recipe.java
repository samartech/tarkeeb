package com.pookidevs.tarkeeb;

import java.util.ArrayList;

public class Recipe {

    private String nameID;
    private String name;
    private String category;
    private String categoryID;
    private int calories;
    private ArrayList<String> ingredients;
    private ArrayList<String> steps;
    private String timeToCook;
    private String serves;
    private String image;

    public Recipe(){

    }



    public Recipe(String name, String category, int calories, ArrayList<String> ingredients, ArrayList<String> steps, String timeToCook, String serves) {
        this.name = name;
        this.category = category;
        this.calories = calories;
        this.ingredients = new ArrayList<String>(ingredients);
        this.steps = new ArrayList<String>(steps);
        this.timeToCook = timeToCook;
        this.serves = serves;
    }

    public Recipe(String name, String category, int calories, ArrayList<String> ingredients, ArrayList<String> steps, String timeToCook, String serves, String nameID, String categoryID) {
        this.nameID = nameID;
        this.name = name;
        this.category = category;
        this.categoryID = categoryID;
        this.calories = calories;
        this.ingredients = ingredients;
        this.steps = steps;
        this.timeToCook = timeToCook;
        this.serves = serves;
    }

    public String getNameID() {
        return nameID;
    }

    public void setNameID(String nameID) {
        this.nameID = nameID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public int getCalories() {
        return calories;
    }

    public void setCalories(int calories) {
        this.calories = calories;
    }

    public ArrayList<String> getIngredients() {
        return ingredients;
    }

    public void setIngredients(ArrayList<String> ingredients) {
        this.ingredients = ingredients;
    }

    public ArrayList<String> getSteps() {
        return steps;
    }

    public void setSteps(ArrayList<String> steps) {
        this.steps = steps;
    }

    public String getTimeToCook() {
        return timeToCook;
    }

    public void setTimeToCook(String timeToCook) {
        this.timeToCook = timeToCook;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getServes() { return serves; }

    public void setServes(String serves) { this.serves = serves; }
}
