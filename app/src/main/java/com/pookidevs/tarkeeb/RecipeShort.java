package com.pookidevs.tarkeeb;

public class RecipeShort {

    private String name;
    private String category;
    private String nameID;
    private String categoryID;
    private String image;

    public RecipeShort(){

    }

    public RecipeShort(String name, String category, String nameID, String categoryID) {
        this.name = name;
        this.category = category;
        this.nameID = nameID;
        this.categoryID = categoryID;
    }

    public RecipeShort(String name, String category) {
        this.name = name;
        this.category = category;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getNameID() {
        return nameID;
    }

    public void setNameID(String nameID) {
        this.nameID = nameID;
    }

    public String getCategoryID() {
        return categoryID;
    }

    public void setCategoryID(String categoryID) {
        this.categoryID = categoryID;
    }

    public String getImage() { return image; }

    public void setImage(String image) { this.image = image; }
}
