package com.pookidevs.tarkeeb

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Button

import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.gms.tasks.Task
import com.google.firebase.FirebaseApp
import com.google.firebase.auth.AdditionalUserInfo
import com.google.firebase.auth.AuthCredential
import com.google.firebase.auth.AuthResult
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.GoogleAuthCredential
import com.google.firebase.auth.GoogleAuthProvider


/**
 * An example full-screen activity that shows and hides the system UI (i.e.
 * status bar and navigation/system bar) with user interaction.
 */
 class LoginActivity:AppCompatActivity() {

        private var mGoogleSignInClient:GoogleSignInClient? = null
        private val RC_SIGN_IN = 10
        private var mAuth:FirebaseAuth? = null
        override fun onCreate(savedInstanceState:Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)

        setContentView(R.layout.activity_login)


 // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        .requestIdToken(getString(R.string.client_id))
        .requestEmail()
        .build()

 // Build a GoogleSignInClient with the options specified by gso.
        mGoogleSignInClient = GoogleSignIn.getClient(this, gso)

 //Fetch google sign in button
        val sign_in_ggl_btn = findViewById<Button>(R.id.btn_signIn)
        sign_in_ggl_btn.setOnClickListener {
            val signInIntent = mGoogleSignInClient!!.signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        }


}

override fun onStart() {
    super.onStart()
     // Check for existing Google Sign In account, if the user is already signed in
     // the GoogleSignInAccount will be non-null.
            val account = GoogleSignIn.getLastSignedInAccount(this)
    if (account != null)
    {
     //TODO: Go to main page
        val typeIntent = Intent(this@LoginActivity, MainActivity::class.java)
        startActivity(typeIntent)

    }

}

 //    public void launch_sign_up_view(View v){
 //        Intent signupIntent = new Intent(LoginActivity.this, SignUpIntermediate.class);
 //        startActivity(signupIntent);
 //    }

    public override fun onActivityResult(requestCode:Int, resultCode:Int, data:Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        // Result returned from launching the Intent from GoogleSignInClient.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN)
        {
        // The Task returned from this call is always completed, no need to attach a listener.
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)
            handleSignInResult(task)
        }
    }

private fun handleSignInResult(completedTask:Task<GoogleSignInAccount>) {
    try
    {
    val account = completedTask.getResult(ApiException::class.java)
    val credential = GoogleAuthProvider.getCredential(account!!.idToken, null)
    mAuth = FirebaseAuth.getInstance()

    mAuth!!.signInWithCredential(credential).addOnCompleteListener(this
    ) { task ->
        if (task.isSuccessful) {
            Log.d("D", "linkWithCredential: Success")

            if (task.result!!.additionalUserInfo!!.isNewUser) {

            }

            val typeSelect = Intent(this@LoginActivity, MainActivity::class.java)
            startActivity(typeSelect)

        } else {
            //TODO: exit app try again later
        }
    }

}
catch (e:ApiException) {
 // The ApiException status code indicates the detailed failure reason.
            // Please refer to the GoogleSignInStatusCodes class reference for more information.
            Log.w("D", "signInResult:failed code=" + e.statusCode)

}

}
}
