package com.pookidevs.tarkeeb

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import com.google.android.material.tabs.TabLayout
import androidx.viewpager.widget.ViewPager
import androidx.appcompat.app.AppCompatActivity
import android.widget.ImageButton
import androidx.drawerlayout.widget.DrawerLayout
import com.pookidevs.tarkeeb.ui.main.SectionsPagerAdapter
import com.google.android.material.navigation.NavigationView
import android.view.MenuItem
import android.widget.TextView
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.firebase.auth.FirebaseAuth

class MainActivity : AppCompatActivity() {

    private lateinit var mAuth: FirebaseAuth

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mAuth = FirebaseAuth.getInstance()

        val sectionsPagerAdapter = SectionsPagerAdapter(this, supportFragmentManager)
        val viewPager: ViewPager = findViewById(R.id.view_pager)
        viewPager.adapter = sectionsPagerAdapter
        val tabs: TabLayout = findViewById(R.id.tabs)
        tabs.setupWithViewPager(viewPager)

        val drawerbtn = findViewById<ImageButton>(R.id.drawerbtn)
        val drawerLayout = findViewById<DrawerLayout>(R.id.drawer_layout)

        drawerbtn.setOnClickListener({
                v->
            if(drawerLayout.isDrawerOpen(Gravity.START)){
                drawerLayout.closeDrawer(Gravity.START)
            }
            else{
                drawerLayout.openDrawer(Gravity.START)
            }
        })


        var nv = findViewById<NavigationView>(R.id.nav_view)
        nv!!.setNavigationItemSelectedListener(object : NavigationView.OnNavigationItemSelectedListener {
            override fun onNavigationItemSelected(item: MenuItem): Boolean {
                val id = item.getItemId()
                when (id) {
//                    R.id.account -> Toast.makeText(this@MainActivity, "My Account", Toast.LENGTH_SHORT).show()
//                    R.id.settings -> Toast.makeText(this@MainActivity, "Settings", Toast.LENGTH_SHORT).show()
                    R.id.nav_sign_out -> signOut()
                    else -> return true
                }
                return true

            }
        })

        var headerView = nv.getHeaderView(0)
        var tv_username = headerView.findViewById<TextView>(R.id.tv_username)
        tv_username.setText(mAuth.getCurrentUser()?.displayName)

    }

    fun signOut(){
        val gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
            .requestEmail()
            .build()

        // Build a GoogleSignInClient with the options specified by gso.
        var mGoogleSignInClient = GoogleSignIn.getClient(this, gso)
        mGoogleSignInClient.signOut()
            .addOnCompleteListener(this) {
                Log.d("D", "Signed out!!!")
                val loginIntent = Intent(this@MainActivity, LoginActivity::class.java)
                startActivity(loginIntent)
            }
        FirebaseAuth.getInstance().signOut()
        val loginIntent = Intent(this@MainActivity, LoginActivity::class.java)
        startActivity(loginIntent)
    }

    override fun onBackPressed() {
        var a = Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a)
    }


}